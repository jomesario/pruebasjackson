// Se especifican los módulos utilizados en la app
var express  = require('express');
var session  = require('express-session');
var MemoryStore = require('session-memory-store')(session);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require("path");
var favicon = require('serve-favicon');
var app      = express();
var port     = process.env.PORT || 8081;
var passport = require('passport');
var flash    = require('connect-flash');
var helmet = require('helmet');
// Configuración incial, conexión a DB y con Passport (módulo para sesiones)

require('./config/passport')(passport); // Enviar la configuración de Passport (/config/passport.js)

// Inicializar nuestra aplicación express
app.use(morgan('dev')); // Muestra todos las peticiones hechas al servidor (GET,POST,DELETE)
app.use(cookieParser()); // Lee las cookies, necesario para auth
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(helmet());
app.use(bodyParser.json());
app.use(cookieParser());
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname,'public','jacksonPlaca.ico')));
app.set('view engine', 'ejs'); // indicamos el módulo ejs para templating

// necesario para passport, indicamos la llave principal para la encriptación
app.use(session({
	secret: 'alarmante',
	resave: true,
	saveUninitialized: true,
	store : new MemoryStore(60*60*12)
} )); // sesión secreta
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


// especificamos a nuestra app el archivo de rutas (/app/routes.js)
require('./app/routes.js')(app, passport); // Carga nuestras rutas y las manda

// Ejecutar aplicación ======================================================================
app.listen(port);
console.log('Servidor en puerto ' + port);

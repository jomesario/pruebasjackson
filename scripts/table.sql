CREATE DATABASE loginexp_pass;

CREATE TABLE users(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` CHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC)
);

CREATE TABLE clientes(
    ID int NOT NULL AUTO_INCREMENT,
    name varchar(50)NOT NULL,
    phone varchar(15),
    email varchar(75),
    id_conektaVeloz varchar(100),
    id_conektaAdrian varchar(100),
    id_conektaJackson varchar(100),
    id_conektaJuan varchar(100),
    street1 varchar (100),
    street2 varchar (100),
    codigo_postal varchar (100),
    libre4 varchar (100),
    libre5 varchar (100),
    libre6 varchar (100),
    libre7 varchar (100),
    libre8 varchar (100),
    primary key(ID)
);

CREATE TABLE orders(
  ID int NOT NULL AUTO_INCREMENT,
  id_conekta varchar(100) NOT NULL,
  id_order varchar(100) NOT NULL,
  empresa varchar(100) NOT NULL,
  monto int NOT NULL,
  razon varchar(140),
  date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ID)
);

CONFIG / database.js

// Se definen todos los módulos a utilizar
var mysql = require('mysql');
var dbconfig = require('../config/database');
var conekta = require ('conekta');
const sgMail = require('@sendgrid/mail');
var flash = require('connect-flash');
//Archivo JSON con las llaves utilizadas
var keys=require ('../keys.json');

//Se crea conexión a DB (especificada en /config/database.js)
//Los querys para crear las tablas están en /scripts/table.sql.
var connection = mysql.createConnection(dbconfig.connection);
connection.query('USE ' + dbconfig.database);

//Seteamos las llaves de Conekta y SendGrid(módulo que cumple la función de
//Simple Mail Transfer Protocol (SMTP) para enviar correos).
sgMail.setApiKey(keys.sendJack);

module.exports = function(app, passport) {
	//Dirección inicial del sitio
	app.get('/', function(req, res) {
		//Se eliminan todas las Cookies y se carga la página principal
		clearCook(res);
		res.render('index.ejs');
	});

	app.get('/index',function(req,res){
		res.render('index.ejs');
	});

	app.get('/datosMonto',function(req,res){
		res.render('datosMonto');
	});

	app.post('/datosMonto',function(req,res){
		//Creamos dos cookies, una para el monto y otra para
		//la razón de pago
		//!!!!!!!!!!!!!!!!!!!!! en centavos
		res.cookie("cookMon",req.body.monto);
		res.cookie("cookRa",req.body.razon);

		res.redirect('/datosTarjeta');
		// switch(req.cookies.cookEmp){
		// 	case "Veloz":
		// 		res.redirect('/datosTarjetaVeloz');
		// 	break;
		// 	case "Adrian":
		// 		res.redirect('/datosTarjetaAdrian');
		// 	break;
		// 	case "Juan":
		// 		res.redirect('/datosTarjetaJuan');
		// 	break;
		// 	case "Jackson":
		// 		res.redirect('/datosTarjetaJackson');
		// 	break;
		// }


	});

	app.get('/datosTarjeta',function(req,res){
		//cargamos la página datosTarjeta indicada
		switch(req.cookies.cookEmp){
			case "Veloz":
				res.render('datosTarjetaVeloz');
			break;
			case "Adrian":
				res.render('datosTarjetaAdrian');
			break;
			case "Juan":
				res.render('datosTarjetaJuan');
			break;
			case "Jackson":
				res.render('datosTarjetaJackson');
			break;
		}

	});

	app.post('/datosTarjeta',function(req,res){
		//Se crea una cookie con el Token de tarjeta
		//devuelto por conekta
		res.cookie('cookTok',req.body.conektaTokenId);
		res.redirect('generarCargo');
	});

	app.get('/pagado',function(req,res){
		let data={
			monto:req.cookies.cookMon,
			razon:req.cookies.cookRa,
			email:req.cookies.cookMail,
			orden:req.cookies.cookOrd
		};
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//Desbloquear al entrar a producción
		sendEmail(req);
		res.render('pagado',{data});
	});

	app.get('/generarCargo',function(req,res){
		//Obtenemos la fecha y hora de la orden
		let hora = getDateTime();
		let data = {monto:req.cookies.cookMon,razon:req.cookies.cookRa,hora:getDateTime()};
		console.log(data);
		//Mandamos esta información y llamamos la página generarCargo
		res.render('generarCargo',{data})
		});

	app.post('/generarCargo',function(req,res){
		//Mandamos crear la orden a través de una promesa
		//().then & ().catch, y posteriormente se mandan
		//los correos correspondientes
		let des = req.cookies.cookRa;
		createOrder(des,req,connection,res)
		.then(function(resultado){
			//en caso exitoso
			console.log(resultado);
			res.redirect('/Pagado')})
		.catch(function(error){
			//en caso fallido
			if (error.object === 'error') {
				console.log('Hubo un error con el pago');
				clearCook(res);
				if (error.details[0].param ==='amount') {
					req.flash('Error','Favor de ingresar un monto a pagar válido e intentarlo nuevamente,'+
										' no se realizó ningún cargo a la tarjeta.');
				}else	if (error.data.charges.data[0].failure_code === 'card_declined') {
					req.flash('Error','Su tarjeta ha sido rechazada,'+
										' no se realizó ningún cargo a la tarjeta proporcionada.'+
										' \n favor de verificar los datos con su banco o intente otro método de pago.');
				}else if(error.data.charges.data[0].failure_code === 'insufficient_funds'){
					req.flash('Error','No cuenta con fondos suficientes para la transacción,'+
										' no se realizó ningún cargo a la tarjeta proporcionada.'+
										' \n favor de verificar los datos  o intente otro método de pago');
				}else{
					req.flash('Error','Hubo un problema al procesar su pago,'+
										' no se realizó ningún cargo a la tarjeta proporcionada.'+
										' \n favor de verificar los datos con su banco o intente otro método de pago.');
				}

				res.redirect('profile');
			}
			//conekta.errors.processing.bank_bindings.declined
			//conekta.errors.processing.bank_bindings.insufficient_funds

		});
	});

	app.get('/clientes', function(req, res) {
		let data ={name :req.user};
		console.log(req.query);
		res.cookie("cookEmp",req.query.button);
		//Se verifica si el usuario ya existe en Conekta, en caso de
		//que ya exista, nos mandará directamente a la página datosMonto
		connection.query("SELECT * FROM clientes WHERE email = ?",[data.name.username],function(err,rows){
			if (err) {
				console.log('Error, no existe');
				console.log(err);
				res.render('clientes', {
					data
				});
			}
			if(rows.length){
				//Si el usuario sí existe en la DB, se extraerá su id_conekta
				//para realizar una orden
				res.cookie("cookMail",data.name.username);
				console.log('previamente registrado');
				console.log(rows[0].id_conektaVeloz);
				console.log('boton');
				console.log(req.query.button);
				console.log('boton2');
				let c1;
				switch(req.query.button){
					case "Veloz":
						c1 = res.cookie('cookId',rows[0].id_conektaVeloz);
						break;
					case "Adrian":
					 	c1 = res.cookie('cookId',rows[0].id_conektaAdrian);
						break;
					case "Juan":
						c1 = res.cookie('cookId',rows[0].id_conektaJuan);
						break;
					case "Jackson":
						c1 = res.cookie('cookId',rows[0].id_conektaJackson);
						break;
				}
				res.setHeader('cook_1',c1);
				res.redirect('datosMonto');

			}else{
				//En caso de no existir,se nos rediccionará a la vista "clientes"
				//para que registre sus datos
				console.log('No existe');
				res.render('clientes',{data});
			}
		});
	});

	app.post('/clientes', function(req, res) {
		//En caso de que el cliente no haya estado registrado
		//en Conekta, se creará y se almacenará en la tabla
		//"clientes" de la DB
		console.log('Post clientes');
		console.log(req.body);
		if (req.body.phone === '') {
		}else{
			createCustomers(req.body,connection,req.user.username,req)
			.then(function(resultado){
				//Si la promesa fue exitosa
	      console.log(resultado);
	      res.render('profile',{message : req.flash('info','Datos actualizados')});
	    	})
			.catch(function(err){
				//Si la promesa fue fallida
				if (err.object ==='error') {
					console.log(err);
					if (err.details[0].param === 'email') {
						res.render('profile',{message:'Favor de brindar una dirección de correo válida'});
					}else if (err.details[0].param ==='name') {
						res.render('profile',{message:'Favor de brindar un nombre válido'});
					}else if (err.details[0].param === 'phone') {
						res.render('profile',{message:'Favor de brindar un número telefónico válido, o únicamente su correo electrónico'});
					}else{
						res.render('profile',{message:'Hubo un error al registrar sus datos'});
					}
				}
	    	});
		}
	});

	app.get('/login', function(req, res) {
		// Carga la página de login, y enviamos error en caso de existir
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	app.get('/seleccionarPago', function(req, res) {
		res.render('seleccionarPago.ejs');
	});

	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/profile', // Rediccionará a perfil
            failureRedirect : '/login', // Rediccionará a la página de registro si hay un error
            failureFlash : true // Permitimos mensajes de Flash
		}),
        function(req, res) {

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });

	app.get('/signup', function(req, res) {
		// Carga la página y manda el error de flash si es que existe
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', // Rediccionará a perfil
		failureRedirect : '/signup', // Rediccionará a registrarse si hay un error
		failureFlash : true // Permitimos los mensajes de Flash
	}));

	app.get('/profile', isLoggedIn, function(req, res) {
		clearCook(res);
		res.render('profile.ejs', {
			user : req.user,
			message:req.flash('Error') // Obtiene la sesión del usuario y la manda al template
		});
	});

	app.get('/logout', function(req, res) {
		//Elimina todas las cookies y sesiones
		//y redirige a la página principal
		clearCook(res);
		req.logout();
		res.redirect('/');
		});

	app.get('*',function(req,res){
		//Error 404
		console.log(req.body);
		console.log(req.query);


		res.render('error404')
	});

};

// ===Funciones utilizadas===

//Verifica si el usuario está loggeado
function isLoggedIn(req, res, next) {

	// si el usuario fue autenticado en la sesión, continúa
	if (req.isAuthenticated())
		return next();

	// si no, mándalos a la página principal
	res.redirect('/');
}

//Crea el cliente en Conekta, y en caso de ser exitoso, manda a llamar a CreateCustomerDB
function createCustomers(body,connection,mail,req){
	console.log('Creando Customer, imprimiendo Body');
	console.log(body);
	switch(req.cookies.cookEmp){
		case "Veloz":
			conekta.api_key=keys.privateVeloz;
			break;
		case "Adrian":
			conekta.api_key=keys.privateAdrian;
			break;
		case "Juan":
			conekta.api_key=keys.privateJuan;
			break;
		case "Jackson":
			conekta.api_key=keys.privateJackson;
			break;
	}

	let promise = new Promise(function(res,rej){
	  let customer = conekta.Customer.create({
	     'name': body.nombreCliente,
	     'email': mail,
	     'phone': body.phone,
	     'payment_sources': [{
	       'type': 'card',
	       'token_id': 'tok_test_visa_4242'
	     }],shipping_contacts:[{
				 phone:body.phone,
				 receiver:body.nombreCliente,
				 address: {
					 street1:body.street1,
					 country:"MX",
					 postal_code:body.cp
				 }
			 }]
	   }, function(err, customer) {
	       if(err){
	         rej(err);
	       }else{
					 console.log('Cliente creado con éxito');
					 console.log('Guardando Cliente en DB...');
	         createCustomerDB(body,connection,customer.toObject(),mail,req);
					 console.log('Actualizando cliente');
					 switch(req.cookies.cookEmp){
						 case "Veloz":
						 	updateCustomers(req,"Adrian",customer.toObject(),mail);
							updateCustomers(req,"Juan",customer.toObject(),mail);
							updateCustomers(req,"Jackson",customer.toObject(),mail);
						break;
						case "Adrian":
							updateCustomers(req,"Veloz",customer.toObject(),mail);
 					 		updateCustomers(req,"Juan",customer.toObject(),mail);
 					 		updateCustomers(req,"Jackson",customer.toObject(),mail);
						break;
						case "Juan":
							updateCustomers(req,"Veloz",customer.toObject(),mail);
 					 		updateCustomers(req,"Adrian",customer.toObject(),mail);
 					 		updateCustomers(req,"Jackson",customer.toObject(),mail);
						break;
						case "Jackson":
							updateCustomers(req,"Veloz",customer.toObject(),mail);
 					 		updateCustomers(req,"Adrian",customer.toObject(),mail);
 					 		updateCustomers(req,"Juan",customer.toObject(),mail);
						break;
					 }

	         res('cliente agregado');
	       }
	   });
	});

	return promise;
}

//Crea los otro usuario en las otras empresas de Conekta a partir de un nombre
function updateCustomers(req,nombreEmp,customerIn,mail){

	console.log('intentado update');
	console.log(req.body);
	if (nombreEmp=="Veloz") {
		console.log("Actualizando Veloz");
		console.log(customerIn);
		conekta.api_key=keys.privateVeloz;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'phone': customerIn.phone,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }],shipping_contacts:[{
					 phone:req.body.phone,
					 receiver:req.body.nombreCliente,
					 address: {
						 street1:req.body.street1,
						 street2:req.body.street2,
						 country:"MX",
						 postal_code:req.body.cp
					 }
				 }]
		   }, function(err, customer) {
		       if(err){
						 console.log('Error actualizando Veloz');
						 console.log(err);
		         rej(err);
		       }else{
						 console.log('Imprimiendo customer Veloz');
						 console.log(customer.toObject());
						 let id = customer.toObject().id;
						 let email = customer.toObject().email;
				 		connection.query("UPDATE clientes SET id_conektaVeloz ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
				 			if (err) {
				 				console.log(err);
				 			}else{
				 				console.log('Agregado el Id Veloz');

				 			}
				 		});
		         res('cliente actualizado Veloz');
		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Adrian") {
		console.log("Actualizando Adrian");
		console.log(customerIn);
		conekta.api_key=keys.privateAdrian;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'phone': customerIn.phone,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }],shipping_contacts:[{
					 phone:req.body.phone,
					 receiver:req.body.nombreCliente,
					 address: {
						 street1:req.body.street1,
						 street2:req.body.street2,
						 country:"MX",
						 postal_code:req.body.cp
					 }
				 }]
		   }, function(err, customer) {
		       if(err){
						 console.log('Error actualizando Adrian');
						 console.log(err);
		         rej(err);
		       }else{
						 console.log('Imprimiendo customer Adrian');
						 console.log(customer.toObject());
						 let id = customer.toObject().id;
						 let email = customer.toObject().email;
				 		connection.query("UPDATE clientes SET id_conektaAdrian ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
				 			if (err) {
				 				console.log(err);
				 			}else{
				 				console.log('Agregado el Id Adrian');

				 			}
				 		});
		         res('cliente actualizado Adrian');
		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Juan") {
		console.log("Actualizando Juan");
		conekta.api_key=keys.privateJuan;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'phone': customerIn.phone,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }],shipping_contacts:[{
					 phone:req.body.phone,
					 receiver:req.body.nombreCliente,
					 address: {
						 street1:req.body.street1,
						 street2:req.body.street2,
						 country:"MX",
						 postal_code:req.body.cp
					 }
				 }]
		   }, function(err, customer) {
		       if(err){
		         rej(err);
		       }else{
							 console.log('Imprimiendo customer Juan');
							 console.log(customer.toObject());
							 let id = customer.toObject().id;
							 let email = customer.toObject().email;
					 		connection.query("UPDATE clientes SET id_conektaJuan ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
					 			if (err) {
					 				console.log(err);
					 			}else{
					 				console.log('Agregado el Id Juan');

					 			}
					 		});
			         res('cliente actualizado Juan');

		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Jackson") {
		console.log("Actualizando Jackson");
		conekta.api_key=keys.privateJackson;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'phone': customerIn.phone,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }],shipping_contacts:[{
					 phone:req.body.phone,
					 receiver:req.body.nombreCliente,
					 address: {
						 street1:req.body.street1,
						 street2:req.body.street2,
						 country:"MX",
						 postal_code:req.body.cp
					 }
				 }]
		   }, function(err, customer) {
		       if(err){
		         rej(err);
		       }else{

							 console.log('Imprimiendo customer Jackson');
							 console.log(customer.toObject());
							 let id = customer.toObject().id;
							 let email = customer.toObject().email;
					 		connection.query("UPDATE clientes SET id_conektaJackson ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
					 			if (err) {
					 				console.log(err);
					 			}else{
					 				console.log('Agregado el Id Jackson');
					 			}
					 		});
			         res('cliente actualizado Jackson');
			        }
		   });
		});
		return promise;
	}
}

//Crea los otro usuario en las otras empresas de Conekta a partir de un nombre
//sin el teléfono
function updateCustomersNoPhone(req,nombreEmp,customerIn,mail){
	if (nombreEmp=="Veloz") {
		console.log("Actualizando Veloz");
		console.log(customerIn);
		conekta.api_key=keys.privateVeloz;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }]
		   }, function(err, customer) {
		       if(err){
						 console.log('Error actualizando Veloz');
						 console.log(err);
		         rej(err);
		       }else{
						 console.log('Imprimiendo customer Veloz');
						 console.log(customer.toObject());
						 let id = customer.toObject().id;
						 let email = customer.toObject().email;
				 		connection.query("UPDATE clientes SET id_conektaVeloz ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
				 			if (err) {
				 				console.log(err);
				 			}else{
				 				console.log('Agregado el Id Veloz');
				 			}
				 		});
		         res('cliente actualizado Veloz');
		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Adrian") {
		console.log("Actualizando Adrian");
		console.log(customerIn);
		conekta.api_key=keys.privateAdrian;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }]
		   }, function(err, customer) {
		       if(err){
						 console.log('Error actualizando Adrian');
						 console.log(err);
		         rej(err);
		       }else{
						 console.log('Imprimiendo customer Adrian');
						 console.log(customer.toObject());
						 let id = customer.toObject().id;
						 let email = customer.toObject().email;
				 		connection.query("UPDATE clientes SET id_conektaAdrian ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
				 			if (err) {
				 				console.log(err);
				 			}else{
				 				console.log('Agregado el Id Adrian');

				 			}
				 		});
		         res('cliente actualizado Adrian');
		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Juan") {
		console.log("Actualizando Juan");
		conekta.api_key=keys.privateJuan;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }]
		   }, function(err, customer) {
		       if(err){
		         rej(err);
		       }else{
							 console.log('Imprimiendo customer Juan');
							 console.log(customer.toObject());
							 let id = customer.toObject().id;
							 let email = customer.toObject().email;
					 		connection.query("UPDATE clientes SET id_conektaJuan ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
					 			if (err) {
					 				console.log(err);
					 			}else{
					 				console.log('Agregado el Id Juan');

					 			}
					 		});
			         res('cliente actualizado Juan');

		       }
		   });
		});
		return promise;
	}else
	if (nombreEmp=="Jackson") {
		console.log("Actualizando Jackson");
		conekta.api_key=keys.privateJackson;
		let promise = new Promise(function(res,rej){
		  let customer = conekta.Customer.create({
		     'name': customerIn.name,
		     'email': mail,
		     'payment_sources': [{
		       'type': 'card',
		       'token_id': 'tok_test_visa_4242'
		     }]
		   }, function(err, customer) {
		       if(err){
		         rej(err);
		       }else{

							 console.log('Imprimiendo customer Jackson');
							 console.log(customer.toObject());
							 let id = customer.toObject().id;
							 let email = customer.toObject().email;
					 		connection.query("UPDATE clientes SET id_conektaJackson ='"+id+ "' WHERE email ='"+email+"'",function(err,result){
					 			if (err) {
					 				console.log(err);
					 			}else{
					 				console.log('Agregado el Id Jackson');
					 			}
					 		});
			         res('cliente actualizado Jackson');
			        }
		   });
		});
		return promise;
	}
}

//Crea el cliente en la DB, esta función solo es llamada por createOrders
function createCustomerDB(body,connection,customer,mail,req){
		switch (req.cookies.cookEmp){
			case 'Veloz':
			connection.query('INSERT INTO clientes SET ? ',
				{
					name:body.nombreCliente,
					phone:body.phone,
					email:mail,
					id_conektaVeloz:customer.id,
					street1:body.street1,
					street2:body.street2,
					codigo_postal:body.cp
				}
				, (err, result) => {
					if (err) {
						console.log('Hubo un error agregando al cliente a la DB');
						console.log(err);
					}else{
						console.log('Cliente guardado en DB');
					}
				});
			break;
			case 'Adrian':
			connection.query('INSERT INTO clientes SET ? ',
				{
					name:body.nombreCliente,
					phone:body.phone,
					email:mail,
					id_conektaAdrian:customer.id,
					street1:body.street1,
					street2:body.street2,
					codigo_postal:body.cp
				}
				, (err, result) => {
					if (err) {
						console.log('Hubo un error agregando al cliente a la DB');
						console.log(err);
					}else{
						console.log('Cliente guardado en DB');
					}
				});
			break;
			case 'Juan':
			connection.query('INSERT INTO clientes SET ? ',
				{
					name:body.nombreCliente,
					phone:body.phone,
					email:mail,
					id_conektaJuan:customer.id,
					street1:body.street1,
					street2:body.street2,
					codigo_postal:body.cp
				}
				, (err, result) => {
					if (err) {
						console.log('Hubo un error agregando al cliente a la DB');
						console.log(err);
					}else{
						console.log('Cliente guardado en DB');
					}
				});
			break;
			case 'Jackson':
			connection.query('INSERT INTO clientes SET ? ',
				{
					name:body.nombreCliente,
					phone:body.phone,
					email:mail,
					id_conektaJackson:customer.id,
					street1:body.street1,
					street2:body.street2,
					codigo_postal:body.cp
				}
				, (err, result) => {
					if (err) {
						console.log('Hubo un error agregando al cliente a la DB');
						console.log(err);
					}else{
						console.log('Cliente guardado en DB');
					}
				});
			break;
		}
	}

//Crea el cliente en la DB sin el teléfono
function createCustomerDBNoPhone(body,connection,customer,mail,req){
	switch (req.cookies.cookEmp){
		case 'Veloz':
		connection.query('INSERT INTO clientes SET ? ',
			{
				name:body.nombreCliente,
				email:mail,
				id_conektaVeloz:customer.id
			}
			, (err, result) => {
				if (err) {
					console.log('Hubo un error agregando al cliente a la DB');
					console.log(err);
				}else{
					console.log('Cliente guardado en DB');
				}
			});
		break;
		case 'Adrian':
		connection.query('INSERT INTO clientes SET ? ',
			{
				name:body.nombreCliente,
				email:mail,
				id_conektaAdrian:customer.id
			}
			, (err, result) => {
				if (err) {
					console.log('Hubo un error agregando al cliente a la DB');
					console.log(err);
				}else{
					console.log('Cliente guardado en DB');
				}
			});
		break;
		case 'Juan':
		connection.query('INSERT INTO clientes SET ? ',
			{
				name:body.nombreCliente,
				email:mail,
				id_conektaJuan:customer.id
			}
			, (err, result) => {
				if (err) {
					console.log('Hubo un error agregando al cliente a la DB');
					console.log(err);
				}else{
					console.log('Cliente guardado en DB');
				}
			});
		break;
		case 'Jackson':
		connection.query('INSERT INTO clientes SET ? ',
			{
				name:body.nombreCliente,
				email:mail,
				id_conektaJackson:customer.id
			}
			, (err, result) => {
				if (err) {
					console.log('Hubo un error agregando al cliente a la DB');
					console.log(err);
				}else{
					console.log('Cliente guardado en DB');
				}
			});
		break;
	}
};

//Crear la Orden en Conekta, y en caso de ser exitoso, manda a llamar a CreateOrderDB
function createOrder(des,req,connection,resp){
	let desc=des;
	console.log(des);
	switch(req.cookies.cookEmp){
		case "Veloz":
			conekta.api_key=keys.privateVeloz;
			break;
		case "Adrian":
			conekta.api_key=keys.privateAdrian;
			break;
		case "Juan":
			conekta.api_key=keys.privateJuan;
			break;
		case "Jackson":
			conekta.api_key=keys.privateJackson;
			break;
	}

	let promise=new Promise(function(res,rej){
		console.log(req.cookies);
			let order=conekta.Order.create({
				'currency': "MXN",
				'customer_info': {
					'customer_id': req.cookies.cookId
				},
				'line_items': [{
					'name': desc,
					'unit_price': req.cookies.cookMon*100,
					'quantity': 1
				}],
				'shipping_lines': [{
		  'amount': 00,
		  'carrier': 'Jacksonalarmas'
	  }],
				'charges': [{
					'payment_method': {
						'token_id':req.cookies.cookTok,
						'type': "card"
					}
				}]
			}, function(err, order) {
				if (err) {
					console.log('Error al procesar pago');
					console.log(err);
					rej(err);
				}else{
					console.log('Guardando en DB...');
					let idOrden=order._id;
					resp.cookie("cookOrd",idOrden);
					let body ={
						id_conekta:req.cookies.cookId,
						monto:req.cookies.cookMon,
						razon:des,
						id_order:idOrden
					};
					createOrderDB(req,body,connection,order);
					res('Orden creada con éxito');
				}
			});
		});
	return promise;

	};

//Guarda la orden en la DB, esta función solo es llamada por CreateOrders
function createOrderDB(req,body,connection,order){
	console.log(body);
	connection.query('INSERT INTO orders SET ? ',
    {
      id_conekta:body.id_conekta,
			id_order:body.id_order,
      monto:body.monto,
			empresa:req.cookies.cookEmp,
      razon:body.razon
    }
  , (err, result) => {
		if (err) {
			console.log('Hubo un error agregando la orden a la DB');
			console.log(err);
		}else{

			console.log('Orden guardada en DB');
		}
  });
}

//Obtiene la fecha y hora del momento de llamada del sistema
function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return day + "/" + month + "/" + year + " " + hour + ":" + min;

}

//Envía correo a usuario y a Jackson
function sendEmail(req){
	console.log(req.cookies.cookOrd);
	let msg = {
	  to: req.cookies.cookMail,
	  from: 'noreply@jackson.com',
	  subject: 'Recibo de pago Jackson Alarmas',
	  text: 'Se registró su pago por $'+req.cookies.cookMon+" MXN, con motivo de pago :"+req.cookies.cookRa+", Muchas gracias por su pago",
	  html: 'Se registró su pago por $'+req.cookies.cookMon+" MXN, con motivo de pago : "+req.cookies.cookRa+", muchas gracias por su pago",
	};
	console.log('Enviando correo a usuario...');
	sgMail.send(msg);

	msg = {
	to: 'fes@velozmonitoreo.com.mx',
	from: 'noreply@jackson.com',
	subject: 'Recibo de pago',
	text: 'El cliente con ID: '+req.cookies.cookId+', realizó un pago por $'+req.cookies.cookMon+".00 MXN, con motivo de pago :"+req.cookies.cookRa+
	", favor de verificarlo en la DB",
	html: 'El cliente con ID: '+req.cookies.cookId+', realizó un pago por $'+req.cookies.cookMon+" MXN, con motivo de pago : "+req.cookies.cookRa+
	+", con una ficha de orden: "+req.cookies.cookOrd+", favor de verificarlo en la DB",
	};
	console.log('Enviando correo a Jackson...');
	sgMail.send(msg);
};

//Elimina todas las cookies que tenga el navegador, excepto el de sessión
function clearCook(res){
	res.clearCookie('cookId');
	res.clearCookie('cookMon');
	res.clearCookie('cookRa');
	res.clearCookie('cookTok');
	res.clearCookie('cookMail');
	res.clearCookie('cookOrd');
	res.clearCookie('cookEmp');
};
